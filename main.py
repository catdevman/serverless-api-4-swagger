#!/usr/bin/env python

from prance import ResolvingParser
from jinja2 import Template
from jinja2.filters import FILTERS, environmentfilter
import argparse
import os
import base64
import ast

@environmentfilter
def b64encode(environment, value, attribute=None):
    return str(base64.b64encode(value.encode('utf8')), 'utf8')

FILTERS["b64encode"] = b64encode

def init_argparse() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(
        usage="%(prog)s [OPTION] [FILE]...",
        description="Create a SAM template from swagger or openapi json file."
    )
    parser.add_argument(
        "-v", "--version", action="version",
        version = f"{parser.prog} version 1.0.0"
    )
    parser.add_argument('input', nargs='*')
    return parser

def main() -> None:
    argparser = init_argparse()
    args = argparser.parse_args()
    parser = ResolvingParser(args.input[0], backend = 'openapi-spec-validator')

    functions = []
    for path in parser.specification['paths']:
        for method in parser.specification['paths'][path]:
            function = parser.specification['paths'][path][method]
            function['path'] = path
            function['method'] = method.upper()
            functions.append(function)

    sam_template = Template("""
AWSTemplateFormatVersion: '2010-09-09'
Transform: AWS::Serverless-2016-10-31
Resources:
{% for function in functions %}
  {{ function.operationId | title}}Lambda:
    Type: AWS::Serverless::Function
    Properties:
      Handler: mapper.handler
      Runtime: python3.8
      CodeUri: ./src
      Events:
        Http{{function.method|title}}:
          Type: Api
          Properties:
            Path: '{{function.path}}'
            Method: '{{function.method|upper}}'
{% endfor %}""")

    os.makedirs("gen/src", exist_ok=True)

    with open("gen/template.yaml", "w") as text_file:
        text_file.write(sam_template.render(functions=functions))

    mapper_template = Template("""
import base64
import json
import handlers

HANDLERS_MAP = {
# base64encode path + method
{% for function in functions %}{% set id = function.path + function.method %}  '{{id | b64encode}}': handlers.{{function.operationId}}{% if not loop.last %},{% endif %}
{% endfor %}}
def handler(event, context):
    print(event)
    try:
        lambda_response = { 'error': 'handler does not defined for this request'}
        resource_method = event['resource'] + event['httpMethod']
        resource_method_encoded = str(base64.b64encode(resource_method.encode('utf8')), 'utf8')
        print(resource_method, resource_method_encoded)
        if resource_method_encoded in HANDLERS_MAP.keys():
            lambda_response =  HANDLERS_MAP[resource_method_encoded](event)
    except Exception as e:
        lambda_response = { 'error': str(e)}

    print(lambda_response)
    return lambda_response""")

    with open("gen/src/mapper.py", "w") as text_file:
        text_file.write(mapper_template.render(functions=functions))

    handlers_template = Template("""import json
{% for function in functions %}
def {{ function.operationId }}(event):
  pass
{% endfor %}""")

    if os.path.isfile("gen/src/handlers.py"):
        with open("gen/src/handlers.py", "r+") as text_file:
            file_str = text_file.read()
            st = ast.parse(file_str)
            for function in functions:
                ast_funcs = [p for p in st.body if isinstance(p, ast.FunctionDef) if p.name == function['operationId']]
                if len(ast_funcs) == 0:
                    print("file is missing the {} function... we should add it".format(function['operationId']))
                    text_file.write("""
def {}(event):
    pass""".format(function['operationId']))
    else:
        with open("gen/src/handlers.py", "w") as text_file:
            text_file.write(handlers_template.render(functions=functions))

if __name__ == '__main__':
    main()
